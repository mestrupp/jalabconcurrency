# Concurrency Group Project #
MPCS 51037: Advanced Java
Instructor: Adam Gerber
Date: August 13th, 2014

Prepared by Daniel Stankiewicz and Michael Strupp.

### Background ###

This repository consists of two separate code branches, which each focus on particular aspects of Java concurrency.

- Branch "dstan" (prepared by Dan) focuses on concurrency related to JavaFX GUI applications.  Also contains an example of the concurrency features of the Stream object for parallel processing.

- Branch "mstrp" (prepared by Mike) focuses on a variety of core concurrency concepts and classes, all written for the Java console.

### Branch DSTAN ###

These examples are meant to show the behavior of Tasks, how to run a ScheduledService, and details on the threading model of Swing vs JavaFX.

To run each example, uncomment the appropriate line in the POM file.


### Branch MSTRP ###

The project consists of 12 "mini" programs, each which detail a particular aspect about Java Concurrency.  These aspects include using the Executor Service, the Fork/Join framework, atomic variables, locks, semaphores, concurrent collections and more.

To run each example, right click on the project in Netbeans --> select the "Custom" menu entry and then you will see links to each executable program. Click on one and it will soon run in the Netbeans console tray.

Please see the short "z_Program_Index.txt" file in the Netbeans project itself for a list of the various programs.

Also note that a Powerpoint presentation discussing these topics is included in /resources/presentation.